#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstring>
using namespace std;

//Librerias
#include "Dijkstra.h"

// Constructor
Dijkstra::Dijkstra() {}

// Inicializaciones
// inicializa un vector. recibe el vector como un puntero.
void Dijkstra::inicializar_vector_caracter(char *vector, int n) {
    int col;
    // Crea el espacio de cada elemento por elemento filasxcolumnas
    // recorre el vector.
    for (col=0; col<n; col++) {
        vector[col] = ' ';
    }
}

// Inicializa matriz nxn. recibe puntero a la matriz.
void Dijkstra::inicializar_matriz_enteros(int **matriz, int n) {
    // Crea el espacio de cada elemento por elemento filasxcolumnas
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            matriz[fila][col] = -1;
        }
    }
}

// inicializa vectores de naturaleza int (en este caso el vector D seria el unico int)
void Dijkstra::inicializar_vector_D(int *D, int **matriz, int n) {
    int col;
    // Crea elemento por elemento por las columnas
    for (col=0; col<n; col++) {
        D[col] = matriz[0][col];
    }
}


// Impresiones
// Imprime un vector el cual recibira como puntero (de naturaleza string/char)
void Dijkstra::imprimir_vector_caracter(char *vector, int n) {
    cout << endl;
    //REcorre e imprime elemento por elemento
    for (int i=0; i<n; i++) {
        cout << "vector[" << i << "]: " << vector[i] << endl;
    }
    
}

// imprime matriz.
void Dijkstra::imprimir_matriz(int **matriz, int n) {
    cout << endl;
    //REcorre e imprime elemento por elemento de la forma nxn
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            cout << matriz[fila][col] << " "; 
        }
        cout << endl;
    }
    cout << endl;
}

// Imprime un vector el cual recibira como puntero (de naturaleza int, en este caso es exclusivo para el D)
void Dijkstra::imprimir_vector_entero(int *vector, int n){
    int i;
    cout << endl;
    //REcorre e imprime elemento por elemento
    for (i=0; i<n; i++) {
        cout << "D["<< i <<"]: " << vector[i] << endl;
    }
    
}



// Llenado/ingreso de los datos a la matriz
// Se agregan los nombres de los nodos a la matriz en un vector nombre[]
void Dijkstra::agregar_nombres(int n, char nombres[], int **matriz) {
    char nombre;
    
    cout << endl;
    for(int i=0; i<n; i++){
        cout << "Ingrese el nombre del nodo " << i+1 << ": ";
        cin >> nombre;
        nombres[i] = nombre;
    }
}

// Se ingresan las distancias de cada nodo (y como interactuan entre ellas)
void Dijkstra::llenar_matriz(int n, char nombres[], int **matriz){ 
    int distancia;
    
    /* Llenar la matriz */
    for (int i=0; i<n; i++){
        cout << endl;
        cout << "Trabajando con el nodo: " << nombres[i];
        cout << endl;
        
        for (int j=0; j<n; j++){
            if(i != j){
                cout << "Ingrese la distancia al nodo " << nombres[j] << ": ";
                cin >> distancia;

                matriz[i][j] = distancia;
            }
            else{
                /* Diagonal de la matriz*/
                matriz[i][j] = 0;
            }
        }
    }
}



// Funciones para el algoritmo
// Se actualiza/agrega informacion al vector S dependiendo de la situacion 
void Dijkstra::agrega_vertice_a_S(char *S, char vertice, int n){
    int i;
    
    // recorre buscando un espacio vacio.
    for (i=0; i<n; i++) {
        if (S[i] == ' ') {
            S[i] = vertice;
            return;
        }
    } 
}

// El vector VS se renueva con nuevos elementos
void Dijkstra::actualizar_VS(char *V, char *S, char *VS, int n) {
    int j;
    int k = 0;
    
    //Se reinicia/re instancia el vector VS para que quede sin informacion
    inicializar_vector_caracter(VS, n);
    
    for (j=0; j<n; j++){
        // por cada caracter de V[] evalua si está en S[],
        // Sino está, lo agrega a VS[].
        if (busca_caracter(V[j], S, n) != true) {
            VS[k] = V[j];
            k++;
        }
    }
}

// Busca la aparición de un caracter en un vector de caracteres.
int Dijkstra::busca_caracter(char c, char *vector, int n) {
    int j;
    
    // Si aparece en el vector en el que se esta buscando, reporta true
    for (j=0; j<n; j++) {
        if (c == vector[j]) {
            return true;
        }
    }
    
    // Si no, retorna falso
    return false;
}

// Se escoge vertice con menos peso/distancia dentro de VS[] y busca el peso en D[]
char Dijkstra::elegir_vertice(char *VS, int *D, char *V, int n){
    int i = 0;
    int menor = 0;
    int peso;
    char vertice;

    while (VS[i] != ' ') {
        // Busca dato
        peso = D[buscar_indice_caracter(V, VS[i], n)];
        // descarta valores infinitos (-1) y 0.
        if ((peso != -1) && (peso != 0)) {
            if (i == 0) {
                menor = peso;
                vertice = VS[i];
            } 
            
            else {
                if (peso < menor) {
                    menor = peso;
                    vertice = VS[i];
                }
            }
        }

        i++;
    }
    
    cout << "\nVertice menor: " << vertice << endl;
    return vertice;

}

// Retorna el índice del caracter consultado.
int Dijkstra::buscar_indice_caracter(char *V, char caracter, int n) {
    int i;
    
    // Va comparando los indices
    for (i=0; i<n; i++) {
        if (V[i] == caracter)
        return i;
    }
    
    return i;
}

// Se actualizan todos los datos para cada nodo/vertice al aplicar algoritmo
void Dijkstra::actualizar_pesos(int *D, char *VS, int **matriz, char *V, char v, int n) {
    int i = 0;
    int indice_w, indice_v;

    cout << "\n> actualiza pesos en D[]" << endl;
    
    indice_v = buscar_indice_caracter(V, v, n);
    while (VS[i] != ' ') {
        if (VS[i] != v) {
            indice_w = buscar_indice_caracter(V, VS[i], n);
            D[indice_w] = calcular_minimo(D[indice_w], D[indice_v], matriz[indice_v][indice_w]);
        }
        i++;
    }
}

// Util para calcular pesos
int Dijkstra::calcular_minimo(int dw, int dv, int mvw) {
    int min = 0;

    if (dw == -1) {
        if (dv != -1 && mvw != -1)
        min = dv + mvw;
        else
        min = -1;

    } 

    else {
        if (dv != -1 && mvw != -1) {
            if (dw <= (dv + mvw)){
                min = dw;
            }
            else{
                min = (dv + mvw);
            }
        }
        else{
            min = dw;
        }
    }
    
    cout << "\ndw: " << dw;  
    cout << "\ndv: " << dv;
    cout << "\nmvw: " << mvw;
    cout << "\nmin: " << min;
    cout << endl;

    return min;
}




// Algoritmo dijkstra
// EL algoritmo entero 
void Dijkstra::aplicar_dijkstra(char *V, char *S, char *VS, int *D, int **matriz, int n){
    
    int i;
    char v;
    
    // inicializar vector D[] segun datos de la matriz M[][] 
    cout << "\n> Se inicializa el vector D" << endl;
    inicializar_vector_D(D, matriz, n);
    cout << endl;
    imprimir_vector_entero(D, n);

    // IMpresion de datos antes de aplicar el algoritmo
    cout << "\n> IMpresion de datos antes de aplicar el algoritmo" << endl;
    cout << "\nVector V: ";
    imprimir_vector_caracter(V, n);
    cout << "\nVector S: ";
    imprimir_vector_caracter(S, n);
    cout << "\nVector VS: ";
    imprimir_vector_caracter(VS, n); 

    // agrega primer véctice.
    cout << "\n> Se agrega el primer valor V[0] a S[] y actualiza VS[]" << endl;
    agrega_vertice_a_S(S, V[0], n);
    cout << "\nVector S: ";
    imprimir_vector_caracter(S, n);
    actualizar_VS(V, S, VS, n);

    cout << "\nVector VS: ";
    imprimir_vector_caracter(VS, n);
    cout << "\nVector VS: ";
    imprimir_vector_entero(D, n);

    for (i=1; i<n; i++) {
        // elige un vértice en v de VS[] tal que D[v] sea el mínimo 
        cout << "\n> Se elige el vertice menor en VS[] según valores en D[] y lo agrega a S[] y actualiza VS[]" << endl;
        v = elegir_vertice(VS, D, V, n);

        agrega_vertice_a_S(S, v, n);
        imprimir_vector_caracter(S, n);

        actualizar_VS(V, S, VS, n);
        imprimir_vector_caracter(VS, n);

        actualizar_pesos(D, VS, matriz, V, v, n);
        imprimir_vector_entero(D, n);
    }
    cout << endl;

} 


// Impresion y creaciond el grafo
void Dijkstra::imprimir_grafo(int **matriz, char *vector, int n) {
    int i, j;
    FILE *fp;
    
    fp = fopen("grafo.txt", "w");
    fprintf(fp, "%s\n", "digraph G {");
    fprintf(fp, "%s\n", "graph [rankdir=LR]");
    fprintf(fp, "%s\n", "node [style=filled fillcolor=yellow];");
    
    for (i=0; i<n; i++) {
        for (j=0; j<n; j++) {
        // evalua la diagonal principal.
            if (i != j) {
                if (matriz[i][j] > 0) {
                    fprintf(fp, "%c%s%c [label=%d];\n", vector[i],"->", vector[j], matriz[i][j]);
                }
            }
        }
    }
    
    fprintf(fp, "%s\n", "}");
    fclose(fp);

    system("dot -Tpng -ografo.png grafo.txt");
    system("eog grafo.png &");
}
