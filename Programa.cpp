//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

#include "Dijkstra.h"

// Funcion principal del programa
int main(int argc, char **argv) {

    // Variables y objetos
    int n;
    Dijkstra dijkstra;

    // Convierte string a entero.
    n = atoi(argv[1]);
    
    // Valida cantidad de parámetros mínimos.
    if (n <= 2) {
        cout << "Uso: \n./matriz n" << endl;
        return -1;
    }

    // Instancian vectores (enteros(D) y string(V, S, VS))
    char V[n], S[n], VS[n];
    int D[n];

    // Inicializa vectores.
    dijkstra.inicializar_vector_caracter(V, n);
    dijkstra.inicializar_vector_caracter(S, n);
    dijkstra.inicializar_vector_caracter(VS, n);

    // Creacion de la matriz y el llenado de sus elementos (nxn de enteros).
    int **matriz;
    matriz = new int*[n];
    
    for(int i=0; i<n; i++){
        matriz[i] = new int[n];
    }
    
    dijkstra.inicializar_matriz_enteros(matriz, n);
    dijkstra.agregar_nombres(n, V, matriz);
    dijkstra.llenar_matriz(n, V, matriz);
    dijkstra.imprimir_matriz(matriz, n);

    // Ya con la matriz impresa, se aplica el algoritmo de dijkstra 
    dijkstra.aplicar_dijkstra(V, S, VS, D, matriz, n);
    
    // Se crea e imprime el grafo (.png)
    dijkstra.imprimir_grafo(matriz, V, n);

    return 0;
}