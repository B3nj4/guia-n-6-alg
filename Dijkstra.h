#ifndef DIJKSTRA_H
#define DIJKSTRA_H

//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

// Clase dijkstra para las funciones
class Dijkstra {
    private:
        
    public:
        //Constructor
        Dijkstra();
        
        // Inicializadores
        void inicializar_vector_caracter (char *vector, int n);
        void inicializar_matriz_enteros(int **matriz, int n);
        void inicializar_vector_D(int *D, int **matriz, int n);

        // Impresiones
        void imprimir_vector_caracter(char *vector, int n);  
        void imprimir_matriz(int **matriz, int n);
        void imprimir_vector_entero(int *vector, int n);
        
        // Ingreso de datos a al matriz
        void agregar_nombres(int n, char nombres[], int **matriz);
        void llenar_matriz(int n, char nombres[], int **matriz);

        // Funciones para el algoritmo
        void agrega_vertice_a_S(char *S, char vertice, int n); // vertice char
        void actualizar_VS(char *V, char *S, char *VS, int n);
        int busca_caracter(char c, char *vector, int n);
        char elegir_vertice(char *VS, int *D, char *V, int n); // vertice de char a int
        int buscar_indice_caracter(char *V, char caracter, int n);
        void actualizar_pesos(int *D, char *VS, int **matriz, char *V, char v, int n);
        int calcular_minimo(int dw, int dv, int mvw);

        // Algoritmo
        void aplicar_dijkstra(char *V, char *S, char *VS, int *D, int **matriz, int n);

        // Creacion e impresion del grafo (.png)
        void imprimir_grafo(int **matriz, char *vector, int n);

};
#endif